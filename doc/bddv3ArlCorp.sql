-- MySQL dump 10.13  Distrib 5.7.19, for Win64 (x86_64)
--
-- Host: localhost    Database: arlcorp
-- ------------------------------------------------------
-- Server version	5.7.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `eleve`
--

DROP TABLE IF EXISTS `eleve`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eleve` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prenom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pseudo` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mot_de_passe` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stage` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `roles` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eleve`
--

LOCK TABLES `eleve` WRITE;
/*!40000 ALTER TABLE `eleve` DISABLE KEYS */;
INSERT INTO `eleve` VALUES (2,'Lisa','Olm','Lalice','Manobanee','0',''),(3,'i','want','toBE','A GOOD ONE','0',''),(4,'lala','lala','lala','$2y$13$xm1K8aglnrX9x4vo.Syrr.UERAQjgpAHKIuM5BRpMqLvVkEzWcZae','1','N;'),(5,'lp','lp','lp','$2y$13$5UfPWIiRWSoR9/HPCLDkCu2kuBN3Rr2IHRoWHXfJUK9Dij8YGjgga','lp','N;'),(6,'lk','lk','lk','$2y$13$Nj6nsTRBLsJagnv2sEew5.EOQ3KunfCnWta3qL.LRC/Kcu26/x1QO',NULL,'a:1:{i:0;s:9:\"ROLE_USER\";}'),(7,'lm','lm','lm','$2y$13$aewegtL3t/7ToGtda3AL8.TC8JqL12/c.lTTIAaf6KWip8JZlpf0.',NULL,'a:2:{i:0;s:9:\"ROLE_USER\";i:1;s:10:\"ROLE_ADMIN\";}'),(8,'lp','lp','lp','$2y$13$ft5PkjlKc6WN51gf6PnPKuofsaD2rmhUdWPTsyjFhGa8QVPLhvhHa',NULL,'a:2:{i:0;s:10:\"ROLE_ADMIN\";i:1;s:10:\"ROLE_ADMIN\";}'),(9,'LQQ','LO','LO','$2y$13$Ry75wqFIHV1cOQrfYSQ9H.IvCt33K6YjmUdB68KPbhs78Ak0z7rPW',NULL,'a:1:{i:0;s:10:\"ROLE_ADMIN\";}'),(10,'lj','lj','lj','$2y$13$y4eUFFawf143q40IhLirDuEivuX8B33xIZufJUHdisyRBtnEOBdQC',NULL,'a:1:{i:0;s:9:\"ROLE_USER\";}'),(11,'anthony','anthony','anthony','$2y$13$jyCRBjEU1JLWMa4sOSGhQuJBzYjtyTJckw9aQCujAYlYxhFMwbiiK',NULL,'a:1:{i:0;s:9:\"ROLE_USER\";}');
/*!40000 ALTER TABLE `eleve` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eleve_parent_eleve`
--

DROP TABLE IF EXISTS `eleve_parent_eleve`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eleve_parent_eleve` (
  `eleve_id` int(11) NOT NULL,
  `parent_eleve_id` int(11) NOT NULL,
  PRIMARY KEY (`eleve_id`,`parent_eleve_id`),
  KEY `IDX_98A85351A6CC7B2` (`eleve_id`),
  KEY `IDX_98A8535195A16B63` (`parent_eleve_id`),
  CONSTRAINT `FK_98A8535195A16B63` FOREIGN KEY (`parent_eleve_id`) REFERENCES `parent_eleve` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_98A85351A6CC7B2` FOREIGN KEY (`eleve_id`) REFERENCES `eleve` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eleve_parent_eleve`
--

LOCK TABLES `eleve_parent_eleve` WRITE;
/*!40000 ALTER TABLE `eleve_parent_eleve` DISABLE KEYS */;
/*!40000 ALTER TABLE `eleve_parent_eleve` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entreprise`
--

DROP TABLE IF EXISTS `entreprise`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entreprise` (
  `id` int(11) NOT NULL,
  `nom` varchar(38) NOT NULL,
  `ville` varchar(24) NOT NULL,
  `cp` int(11) NOT NULL,
  `adresse` varchar(44) NOT NULL,
  `mail` varchar(27) DEFAULT NULL,
  `tel` varchar(16) DEFAULT NULL,
  `activite` varchar(36) NOT NULL,
  `active` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entreprise`
--

LOCK TABLES `entreprise` WRITE;
/*!40000 ALTER TABLE `entreprise` DISABLE KEYS */;
INSERT INTO `entreprise` VALUES (1,'Ecole Maternelle Françoise Dolto','Melun',77000,'30 avenue Georges Pompidou',NULL,'01 64 64 04 20','Enseignement',1),(2,'Groupe Adéquat','Vert-Saint-Denis',77240,'390 avenue Anna Lindh','agence.vsd@groupeadequat.fr','06 46 64 27 82','Autre',1),(3,'Le Pain Gourmand','Melun',77000,'47 avenue Georges Pompidou',NULL,'07 50 60 12 06','commerce/Distribution',1),(4,'Franco Pizz','Melun',77000,'14 route de Montereau',NULL,'01 64 37 55 07','Commerce/Distribution',1),(5,'Papa Grill','Melun',77000,'26 Boulevard Gambetta',NULL,'01 60 68 27 01','Hôtellerie/Restauration',1),(6,'Ecole Primaire Jules Ferry','Melun',77000,'Rue Jules Ferry',NULL,'01 64 38 99 78','Enseignement',1),(7,'Tribunal de Grande Instance','Melun',77010,'2 avenue du Général Leclerc',NULL,'01 64 79 81 64','Droit/Justice',1),(8,'Baildy Group','Melun',77000,'1 place Loïc Baron',NULL,'01 60 66 88 50','Multimedia/Audiovisuel/Informatique',1),(9,'Relay Gare','Melun',77000,'Place Callieni',NULL,'01 64 39 33 21','Commerce/Distribution',1),(10,'Micro Crèche Les P’tis Petons','St-Germain-Laxis',77950,'3 bis rue de l’église',NULL,'09 82 42 27 12','Sciences humaines',1),(11,'Diarra Institut','Melun',77000,'Mail Gaillardon',NULL,'01 64 09 81 17','Commerce/Distribution',1),(12,'Menuiserie','Corbeil Essonnes',91100,'21 rue du Champ d’Epreuves',NULL,'01 60 88 25 59','Industrie',1),(13,'Sodiaal International','Paris Cedex 14',75680,'170 bis Boulevard du Montparnasse',NULL,'01 44 10 90 10','Industrie',1),(14,'Intermarché','Melun',77000,'36 avenue du Général Patton',NULL,'01 60 56 00 90','Commerce/Distribution',1),(15,'Orange','Paris Cedex 14',75012,'187 avenue Daumesnil',NULL,'07 87 16 06 59','Multimedia/Audiovisuel/Informatique',1),(16,'TMC télécommandes mécaniques par câble','Le Mée-Sur-Seine',77350,'Rue Jean-Baptiste Colbert',NULL,'01 60 56 56 56','Transports/Automobile',1),(17,'Sobeca','Vert-Saint-Denis',77240,'581 avenue de l’Europe',NULL,'01 64 52 04 30','Bâtiment/Construction',1),(18,'Carrefour','Evry',91006,'9 avenue du Lac Bois Briard Mermoz 6 Bat. K1',NULL,'01 69 64 64 00','Commerce/Distribution',1),(19,'Banque de France','Paris',75001,'31 rue Croix des Petits Champs',NULL,'01 64 87 67 03','Banque/Assurance',1),(20,'Les Plaisirs Parisiens','Melun',77000,'40 Boulevard de l’Almont',NULL,'01 64 87 67 03','Commerce/Distribution',1),(21,'ND Logistics','Le Coudray-Montceaux',91830,'9-11 rue des Haies Blanches',NULL,'01 69 90 70 26','Logistique',1),(22,'Banque de France','Melun',77000,'24 rue St Ambroise',NULL,'01 64 87 67 00','Banque/Assurance',1),(23,'Mairie de Melun','Melun',77000,'210 rue Paul Doumer',NULL,'01 64 52 33 03','Fonction publique',1),(24,'CAMVS','Dammarie-les-Lys',77190,'297 rue Rousseau Vaudran',NULL,'01 64 79 25 25','Fonction publique',1),(25,'La Banque Postale','Melun',77000,'210 avenue Georges Clémenceau',NULL,'01 64 71 37 00','Banque/Assurance',1),(26,'SMR France','Dammarie-les-Lys',77190,'154 avenue du Lys',NULL,'01 64 79 22 00','Transports/Automobile',1),(27,'PMS Photomike Studio','Rubelles',77950,'Centre Commercial Carrefour Market',NULL,'01 64 64 04 23','Multimedia/Audiovisuel/Informatique',1),(28,'Planet Pizza','Melun',77000,'80 bis rue du Général de Gaulle',NULL,'01 60 68 11 11','Commerce/Distribution',1),(29,'Ibis Styles Rubelles','Rubelles',77950,'6 rue du Perré',NULL,'01 64 52 41 41','Hôtellerie/Restauration',1),(30,'XPO SUPPLY CHAIN France','Bretigny-sur-Orge',91220,'Rue de Bourgogne – ZI de la Moinerie',NULL,'01 48 16 38 00','Transports/Automobile',1),(31,'Garage Jean Redele','Brie-Comte-Robert',77170,'17 rue du Général Leclerc',NULL,'01 64 39 95 77','Transports/Automobile',1),(33,'Général d’Optique','Cesson',77240,'Centre Commercial Boissénart',NULL,'01 60 63 01 23','Commerce/Distribution',1),(34,'Euro Disney Associes','Chessy',77700,'1 rue de la Galmy',NULL,NULL,'Art/Spectacle',1),(35,'Elres','Combs-la-Ville',77380,'2 allée René Lalique',NULL,NULL,'Commerce/Distribution',1),(36,'Burton Of London','Melun',77000,'20 rue St Aspais',NULL,'01 64 09 24 82','Commerce/Distribution',1),(37,'Boulangerie Assia','Melun',77000,'12 rue Colonel Picot',NULL,'09 54 77 04 08','Commerce/Distribution',1),(38,'GEMO','Melun',77000,'ZAC du Champs de Foire',NULL,'01 64 71 90 95','Commerce/Distribution',1),(39,'Association Lys ODE77','Dammarie-les-Lys',77190,'7 rue Marc Jacquet',NULL,'01 64 39 97 93','Sciences humaines',1),(40,'Le Petrin de l’Abbaye','Dammarie-les-Lys',77190,'Centre Commercial de l’Abbaye',NULL,NULL,'Commerce/Distribution',1),(41,'Pharmacie Jaoui','Melun',77000,'65 avenue du Général Patton',NULL,'01 60 68 71 00','Santé',1),(42,'Boulangerie l’Aures','Melun',77000,'5-7 Square Blaise Pascal',NULL,NULL,'Commerce/Distribution',1),(43,'Centre DCNS de Nantes','La Montagne',44620,'Indret',NULL,'02 40 84 89 49','Bâtiment/Construction',1),(44,'La Maison de Toutou','Vaux-le-Penil',77000,'25 route de Montereau',NULL,'01 64 37 15 61','Santé',1),(45,'Direction des Affaires Sportives','Melun',77000,'4 rue de la Fontaine Saint Liesne',NULL,'01 60 56 06 20','Fonction publique',1),(46,'SARL VHS','Le Mée-Sur-Seine',77350,'Centre Commercial Plein-Ciel',NULL,NULL,'Commerce/Distribution',1),(47,'Centre Hôspitalier de Melun','Melun',77000,'2 rue Fréteau de Peny',NULL,'01 64 71 60 00','Santé',1),(48,'Hyper U','Brie-Comte-Robert',77170,'Rue Gustave Eiffel',NULL,'01 64 05 23 01','Commerce/Distribution',1),(49,'Musée de la Gendarmerie','Melun',77000,'Avenue du 13eme Dragon',NULL,'01 64 14 54 64','Fonction publique',1),(50,'Boulangerie Pâtisserie Artisanale BAH','Le Mée-Sur-Seine',77350,'Avenue Maurice Dauvergne',NULL,NULL,'Commerce/Distribution',1),(51,'PAK Auto','Vaux-le-Penil',77000,'112 route de Nangis',NULL,'06 64 86 07 07','Transports/Automobile',1),(52,'Boulanger','Cesson',77240,'Rue du Bois des Saint-Pères',NULL,'0 825 85 08 50','Commerce/Distribution',1),(53,'Geolia','Champlan',91160,'3 rue des Clotais',NULL,'09 72 13 20 41','Logistique',1),(54,'Cabinet de Kinésithérapie','Melun',77000,'17 Boulevard François René de Châteaubriand',NULL,'01 64 09 06 96','Santé',1),(55,'Optiko','Melun',77000,'21 Boulevard de l’Almont',NULL,'01 60 66 01 80','Commerce/Distribution',1),(56,'A la Crêperie de l’Avenue','Melun',77000,'18 avenue du Général Patton',NULL,'01 64 52 59 25','Hôtellerie/Restauration',1),(57,'Avancial','Paris',75012,'40 avenue des Terroirs de France',NULL,'01 44 74 95 77','Transports/Automobile',1),(58,'Rigolo comme la Vie','Cesson',77240,'5 rue Aimé Césaire',NULL,'01 64 37 61 94','Sciences humaines',1),(59,'Mairie de Villeneuve-Saint-Georges','Villeneuve-Saint-Georges',94190,'Place Pierre Semard',NULL,'01 43 86 38 00','Fonction publique',1),(60,'D.Lys Burger','Dammarie-les-Lys',77190,'824 avenue du Lys',NULL,'09 83 48 64 88','Hôtellerie/Restauration',1),(61,'Leroy Merlin','Massy',91300,'2 rue Aulnay Dracourt',NULL,'01 69 53 59 59','Commerce/Distribution',1),(62,'Okaïdi','Melun',77000,'28 rue Saint Aspais',NULL,'01 64 14 92 89','Commerce/Distribution',1),(63,'Auchan','Cesson',77240,'Centre Commercial Boissénart',NULL,'01 64 10 16 16','Commerce/Distribution',1),(64,'O’Dwich','Melun',77000,'1 avenue du Général Patton',NULL,'01 75 18 29 02','Hôtellerie/Restauration',1),(65,'Kam Fret Services','Le Mée-Sur-Seine',77350,'261 rue de Chanteloup',NULL,NULL,'Transports/Automobile',1),(66,'Le Moulin à Sakina','Melun',77000,'1 Avenue Patton',NULL,NULL,'Commerce/Distribution',1),(67,'Planète Santé Pharmacie','Melun',77000,'1 Square Blaise Pascal',NULL,'01 60 56 54 44','Santé',1),(68,'Mairie de St-Germain-Laxis','St-Germain-Laxis',77950,'Place Emile Piot',NULL,'01 64 52 27 12','Fonction publique',1),(69,'Sun Paradise','Melun',77000,'13 rue de l’Abreuvoir',NULL,'01 64 09 97 62','Commerce/Distribution',1),(70,'Optique Plein Ciel','Le Mée-Sur-Seine',77350,'Centre Commercial Plein-Ciel',NULL,'01 64 52 29 36','Commerce/Distribution',1),(71,'ID Logistics France','Vert-Saint-Denis',77240,'10 Rue Paul-Henri Spaak',NULL,NULL,'Logistique',1),(72,'Mohammad Emran','Le Mée-Sur-Seine',77350,'49 rue de Bouville',NULL,'09 50 14 91 13','Commerce/Distribution',1),(73,'Mairie de Savigny-le-Temple','Savigny-le-Temple',77176,'1 Place François Mitterrand',NULL,'01 64 10 18 00','Fonction publique',1),(74,'Tati','Melun',77000,'ZAC du Champs de Foire',NULL,'01 60 66 83 83','Commerce/Distribution',1),(75,'Matrechka','Melun',77000,'18 rue Saint Barthélémy',NULL,'06 89 34 08 78','Commerce/Distribution',1),(76,'Multi Accueil AFC Les Petits Bergers','Melun',77000,'Place de la Motte aux Cailles',NULL,'01 64 39 91 61','Sciences humaines',1),(77,'Véronic Coiffure','Vaux-le-Penil',77000,'25 route de Montereau',NULL,'01 64 10 96 71','Commerce/Distribution',1),(78,'BNP Paribas','Melun',77000,'1 rue St Etienne',NULL,'0 820 82 00 01','Banque/Assurance',1),(79,'Préfecture de Seine-et-Marne','Melun',77000,'Rue des Saints-Pères',NULL,'01 64 71 77 77','Fonction publique',1),(80,'Paulstra','Lisses',91090,'24 rue de l’Eglantier',NULL,'01 69 91 50 00','Sport',1),(81,'Vêt Affaires','Savigny-le-Temple',77176,'137 rue de l’Industrie',NULL,'01 75 79 12 67','Commerce/Distribution',1),(82,'Confédération Syndicale des Familles','Melun',77000,'11 Avenue de Saint Exupery',NULL,'01 64 38 51 63','Sciences humaines',1),(83,'Cornet Franck','Broyes',51120,'23 rue Georges Sand',NULL,NULL,'Bâtiment/Construction',1),(84,'Médiathèque','Melun',77000,'25 rue du Château',NULL,'01 60 56 04 70','Fonction publique',1),(85,'EZO','Dammarie-les-Lys',77190,'Place Paul Gauguin',NULL,NULL,'Industrie',1),(86,'Mayeur Garage AD','Moissy-Cramayel',77550,'24 avenue Jean Jaurès',NULL,'01 60 60 63 26','Transports/Automobile',1),(87,'Catchoupiote','Melun',77000,'11 rue du Presbytère',NULL,'01 64 09 48 82','Commerce/Distribution',1),(88,'Bertin & Bertin Avocats Associés','Avon',77210,'2 rue Gambetta',NULL,'01 60 96 89 61','Droit/Justice',1),(89,'Boucherie de l’Almont','Melun',77000,'30 Boulevard de l’Almont',NULL,'01 64 09 20 08','Commerce/Distribution',1),(90,'Ecole Maternelle Montaigu','Melun',77000,'30 avenue Georges Pompidou',NULL,'01 64 52 33 03','Enseignement',1),(91,'Espace 3000 Vesoul','Vesoul',70000,'Rue Victor Dolle',NULL,'03 84 97 10 10','Transports/Automobile',1),(92,'Pierre et Beauté','Melun',77000,'21 rue de St Liesne',NULL,'09 53 70 44 42','Commerce/Distribution',1),(93,'RapidFlore','Melun',77000,'2 avenue du Général Patton',NULL,'01 60 66 47 01','Commerce/Distribution',1),(94,'Lidl','Dammarie-les-Lys',77191,'536 rue des Frères Thibault',NULL,'01 60 56 00 90','Commerce/Distribution',1),(95,'Librairie Jacques Amyot','Melun',77000,'22 rue Paul Doumer',NULL,'01 64 14 44 24','Commerce/Distribution',1),(96,'O.N. BTP','Melun',77000,'26 rue de la Fontaine',NULL,'06 61 07 95 24','Bâtiment/Construction',1),(97,'Lycée Léonard de Vinci','Melun',77011,'2 bis rue Edouard Branly',NULL,'01 60 56 60 60','Enseignement',1),(98,'Picard Surgelés','Savigny-le-Temple',77176,'8 rue de l’Orée du Bois',NULL,'01 60 63 14 14','Commerce/Distribution',1),(99,'Centre Commercial','Pantin',93500,'9 rue du Pré St Gervais',NULL,'01 48 40 17 90','Commerce/Distribution',1),(100,'3H Food','Melun',77000,'Place des 3 Horloges',NULL,'09 81 47 52 37','Hôtellerie/Restauration',1),(101,'Pompes Funèbres Roc-Eclerc','Dammarie-les-Lys',77190,'603 avenue André Ampère',NULL,'01 64 37 21 89','Commerce/Distribution',1),(102,'Publicité Benoist','Vaux-le-Penil',77300,'880 rue du Maréchal Juin',NULL,'01 64 37 15 75','Multimedia/Audiovisuel/Informatique',1),(103,'Maternité','Melun',77011,'2 rue de Fréteau de Peny',NULL,'01 64 71 65 35','Santé',1),(104,'Clinique des Fontaines','Melun',77000,'54 Boulevard Aristide Briand',NULL,'01 60 56 40 00','Santé',1),(105,'Maison Médicale Saint Nicolas','Rubelles',77950,'Rue St Nicolas',NULL,'01 60 66 84 45','Santé',1),(106,'Pharmacie','Dammarie-les-Lys',77190,'Centre Commercial Villaubois',NULL,'01 64 37 36 07','Santé',1),(107,'Auto Ecole','Melun',77000,'16 rue du Colonel Picot',NULL,'09 83 34 06 77','Transports/Automobile',1),(108,'Hôtel Montaigne','Paris',75008,'6 avenue Montaigne',NULL,'01 80 97 40 00','Hôtellerie/Restauration',1),(109,'Salon Fat Beauty','Melun',77000,'12 bis rue St Liesne',NULL,'06 12 78 01 25','Commerce/Distribution',1),(110,'Tonton Grill','Melun',77000,'49 rue St Barthélémy',NULL,'09 83 39 65 39','Hôtellerie/Restauration',1),(111,'Eva & Flo Couture','Melun',77000,'19 rue St Ambroise',NULL,'01 64 39 50 56','Commerce/Distribution',1),(112,'Clinique St Jean','Melun',77000,'41 avenue de Corbeil',NULL,'01 64 14 30 27','Santé',1),(113,'Zadis Electrique','Dammarie-les-Lys',77190,'37 rue Marc Lanvin',NULL,'01 60 65 19 53','Bâtiment/Construction',1),(114,'Centre de loisirs Bois du Lys','Dammarie-les-Lys',77190,'380 chemin du Clocher',NULL,'01 64 37 15 27','Sport',1),(115,'Chantemur','Melun',77000,'ZAC du Champs de Foire',NULL,'01 60 68 25 00','Commerce/Distribution',1);
/*!40000 ALTER TABLE `entreprise` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `etablissement`
--

DROP TABLE IF EXISTS `etablissement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `etablissement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ville` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_postal` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pseudo` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mot_de_passe` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `etablissement`
--

LOCK TABLES `etablissement` WRITE;
/*!40000 ALTER TABLE `etablissement` DISABLE KEYS */;
/*!40000 ALTER TABLE `etablissement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration_versions`
--

DROP TABLE IF EXISTS `migration_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration_versions` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration_versions`
--

LOCK TABLES `migration_versions` WRITE;
/*!40000 ALTER TABLE `migration_versions` DISABLE KEYS */;
INSERT INTO `migration_versions` VALUES ('20181004120456'),('20181004120740'),('20181004121002'),('20181004121307'),('20181004122451'),('20181004123050'),('20181004123342'),('20181004125517'),('20181004130015'),('20181004130512'),('20181004131330'),('20181004131822'),('20181004132238');
/*!40000 ALTER TABLE `migration_versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parent_eleve`
--

DROP TABLE IF EXISTS `parent_eleve`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parent_eleve` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `responsable_legal` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parent_eleve`
--

LOCK TABLES `parent_eleve` WRITE;
/*!40000 ALTER TABLE `parent_eleve` DISABLE KEYS */;
/*!40000 ALTER TABLE `parent_eleve` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `professeur`
--

DROP TABLE IF EXISTS `professeur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `professeur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pseudo` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mot_de_passe` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `professeur`
--

LOCK TABLES `professeur` WRITE;
/*!40000 ALTER TABLE `professeur` DISABLE KEYS */;
INSERT INTO `professeur` VALUES (1,'kpu','kpu','kpu','kpu');
/*!40000 ALTER TABLE `professeur` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stage`
--

DROP TABLE IF EXISTS `stage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_debut` date NOT NULL,
  `date_fin` date NOT NULL,
  `tuteur_id` int(11) NOT NULL,
  `entreprise_id` int(11) NOT NULL,
  `professeur_id` int(11) NOT NULL,
  `eleve_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_C27C9369A6CC7B2` (`eleve_id`),
  KEY `IDX_C27C936986EC68D8` (`tuteur_id`),
  KEY `IDX_C27C9369A4AEAFEA` (`entreprise_id`),
  KEY `IDX_C27C9369BAB22EE9` (`professeur_id`),
  CONSTRAINT `FK_C27C936986EC68D8` FOREIGN KEY (`tuteur_id`) REFERENCES `tuteur` (`id`),
  CONSTRAINT `FK_C27C9369A4AEAFEA` FOREIGN KEY (`entreprise_id`) REFERENCES `entreprise` (`id`),
  CONSTRAINT `FK_C27C9369A6CC7B2` FOREIGN KEY (`eleve_id`) REFERENCES `eleve` (`id`),
  CONSTRAINT `FK_C27C9369BAB22EE9` FOREIGN KEY (`professeur_id`) REFERENCES `professeur` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stage`
--

LOCK TABLES `stage` WRITE;
/*!40000 ALTER TABLE `stage` DISABLE KEYS */;
/*!40000 ALTER TABLE `stage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tuteur`
--

DROP TABLE IF EXISTS `tuteur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tuteur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tuteur`
--

LOCK TABLES `tuteur` WRITE;
/*!40000 ALTER TABLE `tuteur` DISABLE KEYS */;
/*!40000 ALTER TABLE `tuteur` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-10-16 13:46:03
