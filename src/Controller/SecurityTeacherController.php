<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class SecurityTeacherController extends AbstractController
{
    /**
     * @Route("/security/prof", name="security_prof")
     */
    public function index()
    {
        return $this->render('security_prof/listEntreprise.html.twig', [
            'controller_name' => 'SecurityTeacherController',
        ]);
    }
}
