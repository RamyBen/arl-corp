<?php

namespace App\Controller;


use App\Form\FormType;
use App\Entity\Eleve;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Form\Form;


class IndexController extends AbstractController
{

    /** * @Route("/inscription", name= "inscription") */
    public function inscriptionAction(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $eleve = new Eleve();
        $form = $this->createForm(FormType::class, $eleve);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {;
            $password = $passwordEncoder->encodePassword($eleve, $eleve->getPlainPassword());
            $eleve->setPassword($password);
            //on active par défaut
           // $eleve->setIsActive(true);
            //$eleve->addRole("ROLE_ADMIN");
            // 4) save the User!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($eleve);
            $entityManager->flush();
            $this->addFlash('success',
                'Votre compte à bien été enregistré.');
            return $this->redirectToRoute('index');
        }
        return $this->render('index/inscription.html.twig',
            ['form' => $form->createView(), 'mainNavRegistration' => true,
                'title' => 'Inscription']);
    }

//    /** * @Route("/inscriptionProfesseur", name= "inscriptionP") */
//    public function inscriptionProf(Request $request, UserPasswordEncoderInterface $passwordEncoder)
//
//    {
//        $prof = new Professeur();
//        $form = $this->createForm(FormType::class, $prof);
//        $form->handleRequest($request);
//        if ($form->isSubmitted() && $form->isValid()) {;
//            $password = $passwordEncoder->encodePassword($prof, $prof->getPlainPassword());
//            $prof->setPassword($password);
//            //on active par défaut
//            // $prof->setIsActive(true);
//            //$prof->addRole("ROLE_ADMIN");
//
//            $entityManager = $this->getDoctrine()->getManager();
//            $entityManager->persist($prof);
//            $entityManager->flush();
//            $this->addFlash('success',
//                'Votre compte à bien été enregistré.');
//            return $this->redirectToRoute('index');
//        }
//        return $this->render('index/inscription.html.twig',
//            ['form' => $form->createView(), 'mainNavRegistration' => true,
//                'title' => 'Inscription']);
//    }


    /** * @Route("/login", name="login") */
    public function login(Request $request, AuthenticationUtils $authenticationUtils)
    {

        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();
        $form = $this->get('form.factory')
            ->createNamedBuilder(null)
            ->add('_username', null, ['label' => 'Pseudo'])
            ->add('_password', \Symfony\Component\Form\Extension\Core\Type\PasswordType::class, ['label' => 'Mot de passe'])
            ->add('ok', \Symfony\Component\Form\Extension\Core\Type\SubmitType::class, ['label' => 'Ok', 'attr' =>
                ['class' => 'btn-primary btn-block']])
            ->getForm();
        return $this->render('index/login.html.twig', [
            'mainNavLogin' => true, 'title' => 'Connexion',
            'form' => $form->createView(),
            'last_username' => $lastUsername,
            'error' => $error,]);


    }
}