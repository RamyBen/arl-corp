<?php

namespace App\Controller;

use App\Entity\Professeur;
use App\Entity\Stage;
use App\Entity\Tuteur;
use App\Entity\Entreprise;
use App\Entity\Eleve;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\HTTP;

class SecurityAdminController extends AbstractController
{
    /**
     * @Route("/security/admin", name="security_admin")
     */
    public function index()
    {
        return $this->render('security_admin/listEntreprise.html.twig', [
            'controller_name' => 'SecurityAdminController',
        ]);
    }
    //
    //Professeur
    //

    /**
     * @Route("liste/professeur", name="liste_professeur")
     */
    public function listeProfesseur(){
        //Doctrine communication avec la base de données -> comme des objets
        $entityManager = $this->getDoctrine()->getManager();
        //It's responsible for saving objects to, and fetching objects from, the database.

        $em = $entityManager->getRepository(Professeur::class)->findAll();

        foreach ($em as $p) {
            $p->getPrenom();
            $p->getNom();
            $p->getPseudo();
            $p->getMotDePasse();
        }

        $entityManager->flush();

        return $this->render('security_user/listeProfesseur.html.twig',
            array('em' => $em));
    }
    /**
     * @Route("liste/professeur/add", name="add_professeur")
     */
    public function addProfesseur(Request $request){
        $prof = new Professeur();

        $form = $this->createFormBuilder($prof)
            ->add('prenom',TextType::class)
            ->add('nom',TextType::class)
            ->add('pseudo',TextType::class)
            ->add('motDePasse',TextType::class)
            ->add('save',SubmitType::class, array('label'=>'Valider'))
            ->getForm();
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            //$task = $form->getData();
            $em =$this->getDoctrine()->getManager();
            $em->persist($prof);
            $em->flush();
            return $this->redirectToRoute('liste_professeur');
        }
        return $this->render('security_user/ajoutEleve.html.twig',
            array('form' => $form->createView()));
    }
    /**
     * @Route("liste/professeur/delete/{id}", name="delete_professeur")
     */
    public function deleteProfesseur($id){
        $entityManager = $this->getDoctrine()->getManager();
        $prof = $entityManager->getRepository(Professeur::class)->find($id);
        if(!$prof){
            throw $this->createNotFoundException('Professeur N°' .$id. ' introuvable');
        }
        else{
            $entityManager->remove($prof);
            $entityManager->flush();
            return $this->redirectToRoute('liste_professeur');
        }
    }
    /**
     * @Route("liste/professeur/edit/{id}", name="edit_professeur")
     */
    public function editProfesseur(Request $request,$id)
    {
        $prof = $this->getDoctrine()->getRepository(Professeur::class)->find($id);
        $form = $this->createFormBuilder($prof)
            ->add("nom", TextType::class)
            ->add("prenom", TextType::class)
            ->add("pseudo", TextType::class)
            ->add("motDePasse", TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Valider'))
            ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($prof);
            $em->flush();
            return $this->redirectToRoute('liste_professeur');
        }
        return $this->render('security_user/ajoutEleve.html.twig', array('form'=>$form->createView()));
    }

    //
    //Entreprise
    //

    /**
     * @Route("liste/entreprise", name="list_entreprise")
     */
    public function listeEntreprise(Request $request){
//Doctrine communication avec la base de données -> comme des objets
        $entityManager = $this->getDoctrine()->getManager();
        //It's responsible for saving objects to, and fetching objects from, the database.

        $em = $entityManager->getRepository(Entreprise::class)->findAll();

        foreach ($em as $p){
            $p->getNom();
            $p->getVille();
            $p->getCp();
            $p->getAdresse();
            $p->getMail();
            $p->getTel();
            $p->getActivite();
            $p->getActive();
        }

        $entityManager->flush();

        return $this->render('security_user/listEntreprise.html.twig',
            array('em'=> $em,
                'p'=> $p));
    }




    /**
     * @Route("liste/entreprise/add", name="add_entreprise")
     */
    public function addEntreprise(Request $request)
    {
        $product = new Entreprise();
        $product->setNom('');
        $product->setVille('');
        $product->setCp(0);
        $product->setAdresse('');
        $product->setMail('');
        $product->setTel(0);
        $product->setActivite('');
        $product->setActive(0);

        $form = $this->createFormBuilder($product)
            ->add("nom", TextType::class)
            ->add("ville", TextType::class)
            ->add("cp", IntegerType::class)
            ->add("adresse", TextType::class)
            ->add("mail", TextType::class)
            ->add("tel", IntegerType::class)
            ->add("activite", TextType::class)
            ->add("active", IntegerType::class)
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $newEntreprise = $form->getData();
            $this->getDoctrine()->getManager()->persist($newEntreprise);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute("list_entreprise");
        }

        return $this->render('security_user/form.html.twig',  array(
            'form' => $form->createView(),
        ));
    }
    /**
     * @Route("liste/entreprise/edit/{id}", name="update_entreprise")
     */
    public function updateEntreprise($id, Request $request)
    {
        $entrepriseToModif = $this->getDoctrine()->getRepository(Entreprise::class)->find($id);
        $form = $this->createFormBuilder($entrepriseToModif)
            ->add("nom", TextType::class)
            ->add("ville", TextType::class)
            ->add("cp", IntegerType::class)
            ->add("adresse", TextType::class)
            ->add("mail", TextType::class, array('label' => 'mail','required' => false))
            ->add("tel", IntegerType::class)
            ->add("activite", TextType::class)
            ->add("active", IntegerType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $entrepriseToModif = $form->getData();
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute("list_entreprise");
        }

        return $this->render('security_user/form.html.twig', [
            "form" => $form->createView()
        ]);
    }
    /**
     * @Route("liste/entreprise/delete/{id}", name="delete_entreprise")
     */
    public function deleteEntreprise($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entreprise = $entityManager->getRepository(Entreprise::class)->find($id);
        if (!$entreprise) {
            throw $this->createNotFoundException(
                'Entreprise N°'.$id.' Introuvable.'
            );
        }
        else {
            $entityManager->remove($entreprise);
            $entityManager->flush();
            return $this->redirectToRoute('list_entreprise');
        }
    }
    /**
     * @Route("liste/eleve", name="liste_eleve")
     */

    //
    //Elève
    //

    public function listeEleve(Request $request){
        //Doctrine communication avec la base de données -> comme des objets
        $entityManager = $this->getDoctrine()->getManager();
        //It's responsible for saving objects to, and fetching objects from, the database.

        $em = $entityManager->getRepository(Eleve::class)->findAll();

        foreach ($em as $p){
            $p->getPrenom();
            $p->getNom();
            $p->getPseudo();
            $p->getMotDePasse();
        }

        $entityManager->flush();

        return $this->render('security_user/listeEleve.html.twig',
            array('em'=> $em,'p'=> $p));
    }

    /**
     * @Route("liste/eleve/ajout", name="ajout_eleve")
     */
    public function ajoutEleve(Request $request)
    {
        $eleve = new Eleve();

        $form = $this->createFormBuilder($eleve)
            ->add('prenom', TextType::class)
            ->add('nom', TextType::class)
            ->add('pseudo', TextType::class)
            ->add('motDePasse', TextType::class)
            ->add('Register', SubmitType::class, array('label' => 'Valider'))
            ->getForm();

        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            if ($form->isSubmitted() && $form->isValid()) {

                $id = $form['prenom']->getData();
                $un = $form['nom']->getData();
                $deux =$form['pseudo']->getData();
                $trois = $form['motDePasse']->getData();

                $eleve->setPrenom($id);
                $eleve->setNom($un);
                $eleve->setPseudo($deux);
                $eleve->setMotDePasse($trois);


                $em = $this->getDoctrine()->getManager();
                $em->persist( $eleve);
                $em->flush();
                return $this->redirectToRoute('liste_eleve');
            }
        }

        return $this->render('security_user/ajoutEleve.html.twig',
            array('form' => $form->createView()));
    }

    /**
     * @Route("liste/eleve/modifier/{id}", name="maj_eleve")
     */
    public function modifierEleve(Request $request, $id)
    {
        $eleve = $this->getDoctrine()->getRepository(Eleve::class)->find($id);
        if (!$eleve) {
            throw $this->createNotFoundException(
                'No student found for id '.$id
            );
        } else {
            $form = $this->createFormBuilder($eleve)
                ->add('prenom', TextType::class)
                ->add('nom', TextType::class)
                ->add('pseudo', TextType::class)
                ->add('motDePasse', TextType::class)
                ->add('Register', SubmitType::class, array('label' => 'Valider'))
                ->getForm();
        }
        if ($request->isMethod('POST')) {
            $form->submit($request->request->get($form->getName()));
            if ($form->isSubmitted() && $form->isValid()) {
                $item = $form->getData();
                $eleve=$this->getDoctrine()->getManager();
                $eleve->persist($item);
                $eleve->flush();
                return $this->redirectToRoute('liste_eleve');
            }

        }
        return $this->render('security_user/majEleve.html.twig',
            array('form' => $form->createView(),));

    }

    /**
     * @Route("liste/eleve/supprimer/{id}", name="eleve_delete")
     */
    public function deleteEleve($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $eleve = $entityManager->getRepository(Eleve::class)->find($id);

        if (!$eleve) {
            throw $this->createNotFoundException(
                'No student found for id '.$id
            );
        }
        $entityManager->remove($eleve);
        $entityManager->flush();

        return $this->redirectToRoute('liste_eleve',[
            'id' => $eleve->getId()
        ]);
    }

    /**
     * @Route("stage/{id}", name="add_stage")
     */
    public function addStage(Request $request, UserInterface $user)
    {
        $userId=$user->getId();
        $stage = new Stage();
        $stage->setEleve($userId);
        $form = $this->createFormBuilder($stage)
            ->add('dateDebut',DateType::class)
            ->add('dateFin',DateType::class)
            ->add('save',SubmitType::class, array('label'=>'Ajout_Tuteur'))
            ->getForm();
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            //$task = $form->getData();
            $em =$this->getDoctrine()->getManager();
            $em->persist($stage);
            $em->flush();
            return $this->redirectToRoute('ajout_tuteur');
        }
        return $this->render('security_user/ajoutEleve.html.twig',
            array('form' => $form->createView(), 'userId'=>$userId));
    }

    /**
     * @Route("liste/entreprise/stage/tuteur/{id}", name="ajout_tuteur")
     */
    public function addTuteur(Request $request, $id)
    {
        $tuteur = new Tuteur();

        $form = $this->createFormBuilder($tuteur)
            ->add('nom',TextType::class)
            ->add('prenom',TextType::class)
            ->add('statut',TextType::class)
            ->add('mail',TextType::class)
            ->add('tel',TextType::class)
            ->add('save',SubmitType::class, array('label'=>'DeclarationStage'))
            ->getForm();
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            //$task = $form->getData();
            $em =$this->getDoctrine()->getManager();
            $em->persist($tuteur);
            $em->flush();
            return $this->redirectToRoute('list_entreprise');
        }
        return $this->render('security_user/ajoutEleve.html.twig',
            array('form' => $form->createView()));
    }
}
